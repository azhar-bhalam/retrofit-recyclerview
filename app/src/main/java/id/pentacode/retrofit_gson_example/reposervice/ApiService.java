package id.pentacode.retrofit_gson_example.reposervice;

import java.util.ArrayList;

import id.pentacode.retrofit_gson_example.models.ContactList;
import id.pentacode.retrofit_gson_example.models.Movie;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    /*
    Retrofit get annotation with our URL
    And our method that will return us the List of ContactList
    */
    @GET("/course-apis/recyclerview/movies")
    Call<ArrayList<Movie>> getContacts();
}
